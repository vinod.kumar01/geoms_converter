# -*- coding: utf-8 -*-
"""
Created on Wed Jan 13 16:42:13 2021

@author: Vinod
"""
from os import path
import yaml
from toolbox.geoms_create_hdf import convert_qdoas2geoms

site = 'Mohali'
# tracer_name = 'hcho'   # hcho, no2_vis
for tracer_name in 'hcho', 'no2_vis':
    with open('configuration/config_' + site + '.yml') as conf:
        site_prop = yaml.safe_load(conf)
    datadir = site_prop['datadir']
    # for period in ['2019_05', '2019_06', '2019_07', '2019_08', '2019_09',
    #                '2019_10', '2019_11', '2019_12', '2020_01', '2020_02',
    #                '2020_03', '2020_04', '2020_05', '2020_06', '2020_07',
    #                '2020_08', '2020_09', '2020_10', '2020_11', '2020_12',
    #                '2021_01', '2021_02', '2021_03', '2021_04', '2021_05',
    #                '2021_06', '2021_07']:
    for period in ['2022_04']:
        qdoas_file = path.join(datadir, period, period+'.ASC')
        df = convert_qdoas2geoms(qdoas_file, site, tracer_name, testplot=True,
                                 skip_qdoas_rows=1)
