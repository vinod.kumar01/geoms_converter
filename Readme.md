GEOMS Converter
====================
This package contains the python script for converting the QDOAS ASCII output to hdf5 format which complies to the format requirement for GEOMS and NIDFORVAL comparison. The current template can be found [here](https://avdc.gsfc.nasa.gov/index.php?site=596748165#current_template) under the heading _'GEOMS-TE-UVVIS-DOAS-OFFAXIS-GAS-VA'_.
This complainace can also be checked at [AVDC website](https://avdc.gsfc.nasa.gov/index.php?site=1473794367) .
This converter also satisfies the requirement for conversion of dSCD at 30 degree elevation angle to the VCD using lookup table of airmass factors provided for viewing geometry and measurement conditions.

Whats New
--------
11.06.2021: Added complainace with [HARP](https://stcorp.github.io/harp/doc/html/index.html).

10.11.2020: Calculation of start time and end time of measurements if not included in QDOAS output.

Requirements
--------
- [Python 3.x](https://www.python.org/)
- [Numpy](https://numpy.org/)
- [Pandas](https://pandas.pydata.org/)
- [Scipy](https://www.scipy.org/)
- [PyYAML](https://pypi.org/project/PyYAML/)
- [Geopy](https://pypi.org/project/geopy/)
- [h5py](https://www.h5py.org/)
- [Matplotlib](https://matplotlib.org/) (Optional for overview plot)
- Site specific hdf attribute file (See template)
- Configuration file describing the path of lookup tables for dAMF, AVK, Aerosol climatology, Boundary layer height climatology, and standard atmosphere properties.
- Spectral analysis ascii file produced by QDOAS

Recipe for use
--------
Analyse the MAX-DOAS spctra using QDOAS.

Check for the file paths in configuration->config_[site].yml

Run example_[site].py

Here I show an example flowchart of this converter.
```mermaid
graph TD;
  amf[dAMF LUT] --> converter[GEOMS converter];
  qdoas[Spectral analysis ascii file] --> converter[GEOMS converter];
  AVK[AVK LUT] --> converter[GEOMS converter];
  converter[GEOMS converter] --> output[Output HDF file];
```

Contact:
[Vinod Kumar](mailto:vinod.kumar@mpic.de?subject=GEOMS_converter_gitlab)