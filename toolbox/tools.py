# -*- coding: utf-8 -*-
"""
Created on Wed Jul 22 15:20:40 2020

@author: Vinod
"""
import pandas as pd
import yaml
import numpy as np
import h5py
from geopy import Point
from geopy.distance import geodesic
from datetime import datetime, timedelta
import warnings


class GEOMS_converter_Error(Exception):
    '''
    Define generic GEOMS converter exception
    '''

    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)


class GEOMS_converter_Warning(Warning):
    '''
    Define generic GEOMS converter warning
    '''

    def __init__(self, value):
        self.value = value
        warnings.warn(self, stacklevel=2)

    def __str__(self):
        return repr(self.value)


warnings.simplefilter('always', GEOMS_converter_Warning)


class tracer_prop():
    def __init__(self, tracer):
        self.tracer = tracer
        self.hdfvar = tracer['hdfvar']
        self.name = tracer['name']
        self.fit_name = tracer['fit_name']
        self.qdoas_name = tracer['qdoas_name']
        self.rms_th = float(tracer['rms_th'])
        self.mapa_spec = tracer['mapa_spec']
        self.dSCD_lim = {key: [float(i) for i in val]
                         for key, val in tracer['dSCD_lim'].items()}
        self.vcd_lim = [float(i) for i in tracer['vcd_lim']]
        self.conc_lim = [float(i) for i in tracer['conc_lim']]
        self.vmr_lim = [float(i) for i in tracer['vmr_lim']]
        self.vcd_label = tracer['vcd_label']
        self.dSCD_label = tracer['dSCD_label']
        self.conc_label = tracer['conc_label']
        self.vmr_label = tracer['vmr_label']
        self.mol_wt = float(tracer['mol_wt'])
        self.wl = tracer['wl']


def get_tracerdata(tracer_name, site='Mohali'):
    with open('configuration/config_' + site + '.yml') as conf:
        tracer_prop_dict = yaml.safe_load(conf)
    return tracer_prop(tracer_prop_dict[tracer_name])


class qdoas_data():
    def __init__(self, qdoas_file):
        self.qdoas_file = qdoas_file

    def qdoas_loader(self, **kwargs):
        '''
        loads data from QDOAS analysis into a pandas dataframe
        If clean data enables, it will filter out data below RMS threshold
        set in configuration file and above SZA > 85
        '''
        clean_data = kwargs.get('clean_data', True)
        skip_rows = kwargs.get('skip_rows', 0)
        corr_qdoas_saa = kwargs.get('corr_qdoas_saa', True)
        site = kwargs.get('site', 'Mohali')
        df = pd.read_csv(self.qdoas_file, dtype=None, delimiter='\t',
                         skiprows=skip_rows)
        df = df.iloc[:, :-1]
        df['Date_time'] = df['Date (DD/MM/YYYY)'] + df['Time (hh:mm:ss)']
        df['Date_time'] = pd.to_datetime(df['Date_time'],
                                         format='%d/%m/%Y%H:%M:%S')
        df = df.replace(9.9692e+36, np.nan)
        df['Int_time'] = df['Scans']*df['Tint']
        df = df[~df['Int_time'].isna()]
        # special case for Mainz 4Az instrument
        if site == 'Mainz':
            df['Int_time'] = df['Scans']*(df['Scans']*df['Tint']/1000 - 2.6153)
        if 'Start Time (hhmmss)' not in df.columns:
            df['start_time'] = df.apply(lambda x: (x['Date_time'] -
                                        timedelta(seconds=x['Int_time']/2)),
                                        axis=1)
        else:
            df['start_time'] = df.apply(lambda x: x['Date (DD/MM/YYYY)'] +
                                        str(x['Start Time (hhmmss)']).zfill(6),
                                        axis=1)
            df['start_time'] = pd.to_datetime(df['start_time'],
                                              format='%d/%m/%Y%H%M%S')
        if 'Stop Time (hhmmss)' not in df.columns:
            df['stop_time'] = df.apply(lambda x: (x['Date_time'] +
                                       timedelta(seconds=x['Int_time']/2)),
                                       axis=1)
        else:
            df['stop_time'] = df.apply(lambda x: x['Date (DD/MM/YYYY)'] +
                                       str(x['Stop Time (hhmmss)']).zfill(6),
                                       axis=1)
            df['stop_time'] = pd.to_datetime(df['stop_time'],
                                             format='%d/%m/%Y%H%M%S')

        if clean_data:
            for tracer_name in ['no2_vis', 'no2_uv', 'hcho']:
                try:
                    tracer = get_tracerdata(tracer_name, site=site)
                    df.loc[df[tracer.fit_name+'.RMS'] > tracer.rms_th,
                           tracer.qdoas_name] = np.nan
                    df.loc[df['SZA'] > 85, tracer.qdoas_name] = np.nan
                except KeyError:
                    msg = tracer.qdoas_name + ' not found in analysis result'
                    GEOMS_converter_Warning(msg)
                    # print(tracer.qdoas_name + 'not found in analysis result')
        if corr_qdoas_saa:
            df['saa'] = (df['Solar Azimuth Angle']-180) % 360
        return df


def dict2hdf5(filename, input_dict, global_attr=None, var_attr=None):
    """
    @input_dict: input dictionary
    @global_attr: dictionary of global attributes
    """
    with h5py.File(filename, 'w') as h5file:
        if global_attr:
            for key, val in global_attr.items():
                h5file.attrs[key] = val
        recursive_dict2hdf5(h5file, '/', input_dict, var_attr)


def recursive_dict2hdf5(h5file, path, input_dict, var_attr=None):
    """
    Recursively writes variables from hdf file and saves in dictionary
    @h5file: hdf5 file handler as created by h5py.File(...)
    @path: current path in hdf5 file handler
    @input_dict: input dictionary
    """
    for key, item in input_dict.items():
        if not isinstance(key, str):
            key = str(key)
        if isinstance(item, (np.ndarray, np.int64, np.float64, np.str_,
                             str, bytes, float)):
            h5file[path + key] = item
            if (bool(var_attr) & (key in var_attr)):
                for att_key, att_val in var_attr[key].items():
                    h5file[path + key].attrs[att_key] = att_val
        elif isinstance(item, list):
            h5file[path + key] = np.array(item)
            if (bool(var_attr) & (key in var_attr)):
                for att_key, att_val in var_attr[key].items():
                    h5file[path + key].attrs[att_key] = att_val
        elif isinstance(item, dict):
            recursive_dict2hdf5(h5file, path + key + '/',
                                item)
        else:
            raise ValueError('Cannot save %s type' % type(item))


def calc_raa(vaa, saa):
    raa = saa
    raa -= vaa
    raa %= 360
    # RAA lookup table is defined until raa < 180
    if raa > 180:
        raa = 360-raa
    return raa


def sensitiv_lat_lon(origin_lat, origin_lon, azimuth, elev, BLH):
    origin = Point(origin_lat, origin_lon)
    hori_repres = BLH*np.sin(np.deg2rad(elev))
    new_coords = geodesic(kilometers=hori_repres).destination(origin, azimuth)
    new_coords = new_coords.format_decimal()
    lat, lon = [float(i) for i in new_coords.split(',')]
    return lat, lon


def ref_fracday(date):
    time_delta = date - datetime(2000, 1, 1, 0, 0)
    return time_delta.dt.total_seconds() / (24 * 60 * 60)
