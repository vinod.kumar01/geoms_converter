# -*- coding: utf-8 -*-
"""
Created on Tue Jul 21 23:40:16 2020

@author: Vinod
"""
# %% Imports and definitions
from os import path
import numpy as np
from scipy.interpolate import RegularGridInterpolator


def get_nodes(header):
    '''
    Reads header line and return
    nodes of LUT
    '''
    BLH = float(header.split()[8])
    AOD = float(header.split()[12])
    albedo = float(header.split()[16])
    RAA = float(header.split()[21][:-2])
    SZA = float(header.split()[-1][:-2])
    return BLH, AOD, albedo, RAA, SZA


# %% load data
class lut_AVK():
    def __init__(self, **kwargs):
        self.dirname = kwargs.get('dirname',
                                  r'M:\nobackup\donner\forVinod\AMF_LUT_v1.0\AVK_LUT')
        self.tracer = kwargs.get('tracer', 'NO2')
        self.wl = kwargs.get('wl', 360)
        self.filename = '{}_AVK_LUT_{}nm.dat'.format(self.tracer, self.wl)

    def load_data(self):
        '''
        Reads in data from text file of AVK
        '''
        nodes = {}
        header = []
        data = []
        counter = 0
        with open(path.join(self.dirname, self.filename)) as f:
            tempdata = []
            for s in f.readlines():
                s = s.strip()
                if not s:  # empty line
                    header = None
                    continue
                if s.startswith('Wavelength'):  # Header line
                    header.append(s)
                    if counter > 0:
                        data.append(tempdata)
                    tempdata = []
                    counter = 0
                elif s.startswith('Elevation'):
                    continue
                else:
                    tempdata.append([float(i) for i in s.split()[1:]])
                    counter += 1
            data.append(tempdata)
            # add last table
        # soretd is mandatory for regular grid interpolator
        nodes['BLH'] = sorted(list(set([float(i.split()[8])
                                        for i in header])))
        nodes['AOD'] = sorted(list(set([float(i.split()[12])
                                        for i in header])))
        nodes['albedo'] = sorted(list(set([float(i.split()[16])
                                           for i in header])))
        nodes['RAA'] = sorted(list(set([float(i.split()[21][:-2])
                                        for i in header])))
        nodes['SZA'] = sorted(list(set([float(i.split()[-1][:-2])
                                        for i in header])))
        return(nodes, np.array(data))

    def data2dict(self):
        '''
        Create dictionary lookup table from loaded AVK from previous step
        and returns dictionry for look up table
        nodes and linear interpolation function for lookup table
        The interpolator accepts following inputs as tuple in this order
        BLH, AOD, albedo, RAA, SZA, EA, Altitude
        '''
        nodes, data = self.load_data()
        BLH, AOD = nodes['BLH'], nodes['AOD']
        albedo, RAA, SZA = nodes['albedo'], nodes['RAA'], nodes['SZA']
        EA = [10., 15., 20., 30, 35.]
        Alt = [0.025, 0.075, 0.150, 0.250, 0.350, 0.450, 0.625, 0.875, 1.125,
               1.375, 1.750, 2.250, 2.750, 3.500, 4.500, 5.500, 6.500, 7.500,
               8.500, 9.500]
        lut = {h: {} for h in BLH}
        counter = 0
        for blh in BLH:
            for aod in AOD:
                lut[blh][aod] = {}
                for alb in albedo:
                    lut[blh][aod][alb] = {}
                    for raa in RAA:
                        lut[blh][aod][alb][raa] = {}
                        for sza in SZA:
                            lut[blh][aod][alb][raa][sza] = np.zeros((len(EA),
                                                                     len(Alt)))
                            counter += 1
        with open(path.join(self.dirname, self.filename)) as f:
            counter = 0
            for s in f.readlines():
                s = s.strip()
                if not s:  # empty line
                    continue
                if s.startswith('Wavelength'):  # Header line
                    h, aod, alb, raa, sza = get_nodes(s)
                    lut[h][aod][alb][raa][sza] = data[counter, :]
                    counter += 1
        # get ND array at nodes and generate linear interpolation function
        dataND = np.zeros((len(BLH), len(AOD), len(albedo),
                           len(RAA), len(SZA), len(EA), len(Alt)))*np.nan
        for blh_i, blh in enumerate(BLH):
            for aod_i, aod in enumerate(AOD):
                for alb_i, alb in enumerate(albedo):
                    for raa_i, raa in enumerate(RAA):
                        for sza_i, sza in enumerate(SZA):
                            # dataND[i, :] = lut_360[h][aod][alb][raa][sza]
                            data_sel = lut[blh][aod][alb][raa][sza]
                            dataND[blh_i, aod_i, alb_i, raa_i, sza_i, :] = data_sel
        nodes['EA'] = EA
        nodes['Alt'] = Alt
        intp = RegularGridInterpolator((BLH, AOD, albedo, RAA, SZA, EA, Alt),
                                        dataND, bounds_error=False,
                                        fill_value=None)
        return lut, nodes, intp
