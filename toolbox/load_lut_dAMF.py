# -*- coding: utf-8 -*-
"""
Created on Tue Jul 21 17:43:13 2020

@author: Vinod
"""

# %% Imports and definitions
from os import path
import numpy as np
from scipy.interpolate import RegularGridInterpolator


def get_nodes(header):
    '''
    Reads header line and return
    nodes of LUT
    '''
    blh = float(header.split()[8])
    AOD = float(header.split()[12])
    albedo = float(header.split()[16])
    RAA = float(header.split()[-1][:-2])
    return blh, AOD, albedo, RAA


# %% load and process data
class lut_dAMF():
    def __init__(self, **kwargs):
        self.dirname = kwargs.get('dirname',
                                  r'M:\nobackup\donner\forVinod\AMF_LUT_v1.0\DAMF_LUT')
        self.tracer = kwargs.get('tracer', 'NO2')
        self.wl = kwargs.get('wl', 360)
        self.filename = '{}_DAMF_LUT_{}nm.dat'.format(self.tracer, self.wl)

    def load_data(self):
        '''
        Reads in data from text file of dAMFs
        '''
        nodes = {}
        header = []
        data = []
        counter = 0
        with open(path.join(self.dirname, self.filename)) as f:
            tempdata = []
            for s in f.readlines():
                s = s.strip()
                if not s:  # empty line
                    header = None
                    continue
                if s.startswith('Wavelength'):  # Header line
                    header.append(s)
                    if counter > 0:
                        data.append(tempdata)
                elif s.startswith('Elevation'):
                    tempdata = []
                    counter = 0
                    # reset counter for new table
                else:
                    tempdata.append([float(i) for i in s.split()[1:]])
                    counter += 1
            data.append(tempdata)
            # add last table
        # soretd is mandatory for regular grid interpolator
        nodes['BLH'] = sorted(list(set([float(i.split()[8])
                                        for i in header])))
        nodes['AOD'] = sorted(list(set([float(i.split()[12])
                                        for i in header])))
        nodes['albedo'] = sorted(list(set([float(i.split()[16])
                                           for i in header])))
        nodes['RAA'] = sorted(list(set([float(i.split()[-1][:-2])
                                        for i in header])))
        return(nodes, np.array(data))

    def data2dict(self):
        '''
        Create dictionary lookup table from loaded dAMF from previous step
        and returns dictionry for look up table
        nodes and linear interpolation function for lookup table
        The interpolator accepts following inputs as tuple in this order
        BLH(km), AOD, albedo, RAA, SZA, EA
        '''
        nodes, data = self.load_data()
        BLH, AOD = nodes['BLH'], nodes['AOD']
        albedo, RAA = nodes['albedo'], nodes['RAA']
        EA = [10., 15., 20., 25., 30, 35.]
        SZA = [0., 20., 30., 40., 50, 60., 65., 70., 75.,
               80., 82., 84., 86., 88.]
        lut = {blh: {} for blh in BLH}
        counter = 0
        # create initial dictionary with all zeros
        for blh in BLH:
            for aod in AOD:
                lut[blh][aod] = {}
                for alb in albedo:
                    lut[blh][aod][alb] = {}
                    for raa in RAA:
                        lut[blh][aod][alb][raa] = np.zeros((len(EA), len(SZA)))
                        counter += 1
        # fill in data
        with open(path.join(self.dirname, self.filename)) as f:
            counter = 0
            for s in f.readlines():
                s = s.strip()
                if not s:  # empty line
                    continue
                if s.startswith('Wavelength'):  # Header line
                    h, aod, alb, raa = get_nodes(s)
                    lut[h][aod][alb][raa] = data[counter, :]
                    counter += 1
        # get ND array at nodes and generate linear interpolation function
        dataND = np.zeros((len(BLH), len(AOD), len(albedo),
                           len(RAA), len(EA), len(SZA)))*np.nan
        for blh_i, blh in enumerate(BLH):
            for aod_i, aod in enumerate(AOD):
                for alb_i, alb in enumerate(albedo):
                    for raa_i, raa in enumerate(RAA):
                        # dataND[i, :] = lut_360[h][aod][alb][raa]
                        data_sel = lut[blh][aod][alb][raa]
                        dataND[blh_i, aod_i, alb_i, raa_i, :] = data_sel
        nodes['EA'] = EA
        nodes['SZA'] = SZA
        intp = RegularGridInterpolator((BLH, AOD, albedo, RAA, EA, SZA),
                                       dataND, bounds_error=False,
                                       fill_value=None)
        return lut, nodes, intp
