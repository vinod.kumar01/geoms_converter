# -*- coding: utf-8 -*-
"""
Created on Fri Jul 24 16:58:42 2020

@author: Vinod
"""

# %% imports and defintions
from os import path
import numpy as np
import yaml
from datetime import datetime
import matplotlib.pyplot as plt
from scipy.interpolate import griddata, interp2d
from toolbox import tools
from toolbox.load_lut_dAMF import lut_dAMF
from toolbox.load_lut_AK import lut_AVK

data_type = {np.dtype('S1'): 'STRING', np.dtype('float32'): 'REAL',
             np.dtype('float64'): 'DOUBLE'}


def convert_qdoas2geoms(qdoas_file, site_name, tracer_name, **kwargs):
    '''
    Parameters
    ----------
    qdoas_file : string
        Complete path of the QDOAS analysis result
    site_name : string
        Name of site (Currently Mainz and Mohali)
        New site needs corresponding config file
    tracer_name : string
        Name of tracer. Currently GEOMS consideres NO2 (vis) and HCHO
    Returns
    -------
    hdf5 file in GEOMS output structure

    '''
    elev = kwargs.get('elev', 30)  # Elevation angle to use for VCD calculation
    testplot = kwargs.get('testplot', True)
    save_csv_out = kwargs.get('save_csv_out', False)
    skip_rows = kwargs.get('skip_qdoas_rows', 1)
    # Currently GEOMS data is only prepared for NO2 and HCHO
    valid_list = ['hcho', 'no2_vis', 'no2']
    if tracer_name not in valid_list:
        err_msg = 'Tracer name must be one of [%s]' % (','.join(valid_list))
        raise tools.GEOMS_converter_Error(err_msg)
    # load configuration
    with open('configuration/config_' + site_name + '.yml') as conf:
        site = yaml.safe_load(conf)
    site['altitude'] = site['amsl'] + site['height']
    datadir = site['datadir']
    support_data_dir = site['support_data_dir']
    std_atm_subdir = site['std_atm_subdir']
    amf_subdir = site['amf_subdir']
    avk_subdir = site['avk_subdir']
    tracer = tools.get_tracerdata(tracer_name, site_name)
    hdfvar = tracer.hdfvar
    # Load support data (e.g. BLH, AOD, Reference atmospheric conditions)
    BLH_file = path.join(support_data_dir, 'BLH_climatology',
                         'BLH_climatology_{}.dat'.format(site['name']))
    aod_file = path.join(support_data_dir, 'AOD_climatology',
                         'monthly_diurnal_AOD_{}_{}nm.dat'.format(site['aod_site'], tracer.wl))
    aod_climatology = np.genfromtxt(aod_file, comments='%')
    aod_climatology[aod_climatology == 999] = np.nan
    BLH_climatology = np.genfromtxt(BLH_file, comments='%')
    # Make the data cycle for interpolation between 31 Dec and  1 Jan
    BLH_climatology = np.vstack((BLH_climatology, [BLH_climatology[0, :]]))
    BLH_climatology[-1, :2] = [13, 0]  # circle from Dec to Jan
    # Load attributes to be written in the HDF file
    with open('configuration/GEOMS_attrs_{}.yml'.format(site['name'])) as conf:
        attrs = yaml.safe_load(conf)
    global_attr = attrs[tracer.name]['Global_attrs']
    var_attr = attrs[tracer.name]['Vaiable_attrs']
    # %% Load QDOAS output
    inp_data = tools.qdoas_data(qdoas_file)
    data = inp_data.qdoas_loader(skip_rows=skip_rows, clean_data=True,
                                 site=site_name, corr_qdoas_saa=True)
    data['vaa'] = site['vaa']
    data['raa'] = data.apply(lambda x: tools.calc_raa(x['vaa'], x['saa']),
                             axis=1)
    data['hour'] = data['Date_time'].dt.hour+data['Date_time'].dt.minute/60
    data['month'] = data['Date_time'].dt.month
    df = data[(data['Name'] == elev) & (data['SZA'] < 85)]
    # check if dates are monotonouesly increasing
    if not df['start_time'].is_monotonic:
        err_msg = 'Date time not continuously increasing'
        raise tools.GEOMS_converter_Error(err_msg)
    df = df[~df[tracer.qdoas_name].isnull()]
    # interpolate the AOD for the measurement time
    mm, hh = np.meshgrid(np.arange(1, 13), np.arange(24))
    df['AOD'] = griddata((hh.ravel(), mm.ravel()), aod_climatology.T.ravel(),
                         (df['hour'], df['month']))
    df = df.set_index('Date_time')
    df['AOD'] = df['AOD'].interpolate(method='time', limit_direction='both')
    # interpolate the BLH for the measurement time
    df['BLH'] = griddata((BLH_climatology[:, 0], BLH_climatology[:, 1]),
                         BLH_climatology[:, 2], (df['month'], df['hour']))
    # Sensitivity lat lon coordinates
    lat_s = df.apply(lambda x:
                     tools.sensitiv_lat_lon(site['lat'], site['lon'],
                                            site['vaa'], elev, x['BLH'])[0],
                     axis=1).values
    lat_s = np.tile(lat_s, [20, 1]).T
    lon_s = df.apply(lambda x:
                     tools.sensitiv_lat_lon(site['lat'], site['lon'],
                                            site['vaa'], elev, x['BLH'])[1],
                     axis=1).values
    lon_s = np.tile(lon_s, [20, 1]).T
    # US standard temperature, pressure and column
    std_temp = np.genfromtxt(path.join(support_data_dir, std_atm_subdir,
                                       'TEMPERATURE_IND.txt'))
    std_temp = np.tile(std_temp, [len(df), 1])
    std_press = np.genfromtxt(path.join(support_data_dir, std_atm_subdir,
                                        'PRESSURE_IND.txt'))
    std_press = np.tile(std_press, [len(df), 1])
    std_col = np.genfromtxt(path.join(support_data_dir, std_atm_subdir,
                                      'COLUMN_PARTIAL_IND.txt'))
    std_col = np.tile(std_col, [len(df), 1])
    # %% load  bAMF and Averaging kernel and
    # interpolate for measurement conditions
    # forward model for dAMF
    amf_prop = lut_dAMF(tracer=tracer.name.upper(), wl=tracer.wl,
                        dirname=path.join(support_data_dir, amf_subdir))
    damf_lut, nodes_amf, damf_func = amf_prop.data2dict()
    df['dAMF'] = df.apply(lambda x:
                          damf_func((x['BLH'], x['AOD'], site['albedo'],
                                     x['raa'], x['Name'], x['SZA'])), axis=1)
    df['VCD'] = df.apply(lambda x: 1e-15*x[tracer.qdoas_name]/x['dAMF'],
                         axis=1)
    # df['VCD'][df['VCD'] < 0] = np.nan
    # Format checker raises a warning for VCD < 0
    err_name = tracer.qdoas_name.replace('SlCol', 'SlErr')
    AMF_err_per = 0.22 if tracer.name == 'NO2' else 0.23
    df['VCD_err'] = df.apply(lambda x:
                             np.sqrt((x[err_name]*1e-15*x['dAMF'])**2 +
                                     (AMF_err_per*x['VCD'])**2), axis=1)
    # Ststematic error is 3% for NO2 and 9% for HCHO
    sys_err_per = 0.03 if tracer.name == 'NO2' else 0.09
    df['VCD_err_sys'] = df['VCD'].abs()*sys_err_per
    # forward model for averaging kernel
    avk_prop = lut_AVK(tracer=tracer.name.upper(), wl=tracer.wl,
                       dirname=path.join(support_data_dir, avk_subdir))
    avk_lut, nodes_avk, avk_func = avk_prop.data2dict()
    # Create altitude grids
    alt_bounds = np.array([0.0, 0.05, 0.1, 0.2, 0.3, 0.4, 0.5, 0.75, 1.0, 1.25,
                           1.5, 2.0, 2.5, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0,
                           10.0])
    alt_bounds += 1e-3 * site['altitude']
    altitudes = [(i + j) * 0.5
                 for i, j in zip(alt_bounds[:-1], alt_bounds[1:])]
    dh = alt_bounds[1:] - alt_bounds[:-1]
    # Re-format lower and upper boundary for output
    alt_bounds = np.array([alt_bounds[:-1], alt_bounds[1:]])
    AVK = np.zeros((len(df), len(altitudes)))
    for i, alt in enumerate(altitudes):
        AVK[:, i] = df.apply(lambda x:
                             avk_func((x['BLH'], x['AOD'], site['albedo'],
                                       x['raa'], x['SZA'], x['Name'], alt)),
                             axis=1)
    df.reset_index(inplace=True)
    # replace NaN by fill_value
    for col in ['VCD', 'VCD_err', 'VCD_err_sys']:
        df.loc[df[col].isnull(), col] = -90000.0
    # %% Calculation of a priori vertical profiles
    profile_grid = np.genfromtxt(path.join(support_data_dir, 'profile_shapes',
                                           'altitude_grid_vert_profiles.dat'),
                                 skip_header=1)
    blh = np.array([0.2, 0.5, 1.0, 1.5, 3.0])
    profile_data = np.empty((len(profile_grid), len(blh)), dtype=float)
    for i, blh_now in enumerate(blh):
        blh_text = str(blh_now).replace('.', '')
        profile_fname = 'trace_gas_prof_qa4ecv_BLH'+blh_text+'.dat'
        profile_data[:, i] = np.genfromtxt(path.join(support_data_dir,
                                                     'profile_shapes',
                                                     profile_fname),
                                           skip_header=1)
    # profile_fn = RegularGridInterpolator((profile_grid, blh), profile_data,
    #                                      bounds_error=False, fill_value=None)
    # RegularGridInterpolator yeilds unrealiyetic negative values below 200m
    profile_fn = interp2d(profile_grid, blh, profile_data.T,
                                         bounds_error=False, fill_value=None)
    pc_prof = np.zeros_like(AVK)
    for ts, blh_now in enumerate(df['BLH']):
        profile_now = np.asarray([profile_fn(i, blh_now)
                                  for i in altitudes])[:, 0]
        # partial column profile in molecules cm-2
        pc_prof[ts, :] = profile_now*dh*1e5
    # scaling of partical column profiles
    scale_fac = 1e15*(df['VCD'].values)/np.sum(AVK*pc_prof, axis=1)
    pc_prof_new = np.array([pc_prof[:, i]*scale_fac for i in range(20)]).T
    apriori_vertical_col = np.sum(pc_prof_new, 1)/1e15
    pc_prof_new[pc_prof_new * 1e15 < 0] = -90000.0
    apriori_vertical_col[apriori_vertical_col < -100] = -90000.0
    datastr2_towrite = np.zeros(len(df), dtype='S')
    # %% create directory with evenrything required for final hdf
    savedict = {'DATETIME': tools.ref_fracday(df['Date_time']).values,
                'DATETIME.START': tools.ref_fracday(df['start_time']).values,
                'DATETIME.STOP': tools.ref_fracday(df['stop_time']).values,
                'INTEGRATION.TIME': df['Int_time'].to_numpy(dtype='float32'),
                'LATITUDE.INSTRUMENT': np.asarray([site['lat']],
                                                  dtype='float32'),
                'LONGITUDE.INSTRUMENT': np.asarray([site['lon']],
                                                   dtype='float32'),
                'ALTITUDE.INSTRUMENT': np.array([site['altitude']],
                                                dtype='float32'),
                'ALTITUDE': np.array(altitudes, dtype='float32'),
                'PRESSURE_INDEPENDENT': np.float32(std_press),
                'PRESSURE_INDEPENDENT_SOURCE': datastr2_towrite,
                'TEMPERATURE_INDEPENDENT': np.float32(std_temp),
                'TEMPERATURE_INDEPENDENT_SOURCE': datastr2_towrite,
                'COLUMN.PARTIAL_INDEPENDENT': np.asarray(std_col,
                                                       dtype='float32'),
                'COLUMN.PARTIAL_INDEPENDENT_SOURCE': datastr2_towrite,
                'ALTITUDE.BOUNDARIES': np.array(alt_bounds, dtype='float32').T,
                'ANGLE.SOLAR_ZENITH.ASTRONOMICAL': df['SZA'].to_numpy(dtype='float32'),
                'ANGLE.SOLAR_AZIMUTH': df['saa'].to_numpy(dtype='float32'),
                'ANGLE.VIEW_AZIMUTH': df['vaa'].to_numpy(dtype='float32'),
                'ANGLE.VIEW_ZENITH': df['Name'].to_numpy(dtype='float32'),
                'LATITUDE':  np.array(lat_s, dtype='float32'),
                'LONGITUDE':  np.array(lon_s, dtype='float32'),
                'CLOUD.CONDITIONS': datastr2_towrite,
                'AEROSOL.OPTICAL.DEPTH.TROPOSPHERIC_INDEPENDENT': df['AOD'].to_numpy(dtype='float32'),
                hdfvar+'.COLUMN.TROPOSPHERIC_SCATTER.SOLAR.OFFAXIS': df['VCD'].to_numpy(dtype='float32'),
                hdfvar+'.COLUMN.TROPOSPHERIC_SCATTER.SOLAR.OFFAXIS_UNCERTAINTY.RANDOM.STANDARD': df['VCD_err'].to_numpy(dtype='float32'),
                hdfvar+'.COLUMN.TROPOSPHERIC_SCATTER.SOLAR.OFFAXIS_UNCERTAINTY.SYSTEMATIC.STANDARD': df['VCD_err_sys'].to_numpy(dtype='float32'),
                hdfvar+'.COLUMN.TROPOSPHERIC_SCATTER.SOLAR.OFFAXIS_APRIORI': np.asarray(apriori_vertical_col, dtype='float32'),
                hdfvar+'.COLUMN.TROPOSPHERIC_SCATTER.SOLAR.OFFAXIS_AVK': np.asarray(AVK, dtype='float32'),
                hdfvar+'.COLUMN.PARTIAL_SCATTER.SOLAR.OFFAXIS_APRIORI': np.array(pc_prof_new/1e15, dtype='float32')}
    # adding further attributes
    global_attr['FILE_GENERATION_DATE'] = datetime.strftime(datetime.now(),
                                                            '%Y%m%dT%H%M%SZ')
    global_attr['DATA_START_DATE'] = datetime.strftime(df['start_time'].iloc[0].round('1s'),
                                                       '%Y%m%dT%H%M%SZ')
    global_attr['DATA_STOP_DATE'] = datetime.strftime(df['stop_time'].iloc[-1].round('1s'),
                                                      '%Y%m%dT%H%M%SZ')
    global_attr['FILE_NAME'] = 'groundbased_uvvis.doas.offaxis.'
    global_attr['FILE_NAME'] += hdfvar.lower() + '_mpic002_'+site['name']+'_'
    global_attr['FILE_NAME'] += global_attr['DATA_START_DATE'] + '_'
    global_attr['FILE_NAME'] += global_attr['DATA_STOP_DATE'] + '_'
    global_attr['FILE_NAME'] += global_attr['DATA_FILE_VERSION'] + '.h5'
    global_attr['FILE_NAME'] = global_attr['FILE_NAME'].casefold()
    savename = path.join(datadir, global_attr['FILE_NAME'])

    for key in savedict:
        if key not in ['DATETIME', 'ALTITUDE']:
            global_attr['DATA_VARIABLES'] += key + ';'
        var_attr[key]['VAR_NAME'] = key
        var_attr[key]['VAR_SIZE'] = str(savedict[key].shape[0])
        if len(savedict[key].shape) == 2:
            var_attr[key]['VAR_SIZE'] += ';' + str(savedict[key].shape[1])
        var_attr[key]['VAR_DATA_TYPE'] = data_type[savedict[key].dtype]
        # variable attributes should be array of byte charaters and not unicode
    for key in var_attr.keys():
        if var_attr[key]['VAR_DATA_TYPE'] == 'REAL':
            var_attr[key]['VAR_VALID_MAX'] = np.float32(var_attr[key]['VAR_VALID_MAX'])
            var_attr[key]['VAR_VALID_MIN'] = np.float32(var_attr[key]['VAR_VALID_MIN'])
            var_attr[key]['VAR_FILL_VALUE'] = np.float32(var_attr[key]['VAR_FILL_VALUE'])  
        for key2 in var_attr[key].keys():
            if isinstance(var_attr[key][key2], (str)):
                var_attr[key][key2] = np.array(var_attr[key][key2]).astype(dtype='S')
    global_attr['DATA_VARIABLES'] = global_attr['DATA_VARIABLES'][:-1]
    # Global attributes should be array of byte charaters and not unicode
    for key in global_attr.keys():
        if isinstance(global_attr[key], (str)):
            global_attr[key] = np.array(global_attr[key]).astype(dtype='S')
    tools.dict2hdf5(savename, savedict, global_attr=global_attr,
                    var_attr=var_attr)
    # %% test plots
    if testplot:
        fig, ax = plt.subplots(figsize=[10, 4])
        idx = df['VCD'] >= 0
        ax.plot(df['Date_time'][idx], df['VCD'][idx], label='Modified VCD')
        ax.plot(df['Date_time'][idx], 1e-15*df[tracer.qdoas_name][idx],
                label='Geometric VCD')
        ax.plot(df['Date_time'][idx], apriori_vertical_col[idx], label='a priori VCD')
        ax.set_ylabel(tracer.vcd_label+'*(1e15)')
        ax.set_xlabel('Date and Time (UTC)')
        ax.grid(alpha=0.3)
        ax.legend()
        plt.tight_layout()
        # plt.savefig(path.join(datadir, tracer_name+'_vcd_'+period+'.png'),
        #             format='png', dpi=300)
        # plt.close()
    # write excel output
    if save_csv_out:
        df_out = df[['Date_time', tracer.qdoas_name, 'VCD']]
        df_out[tracer.qdoas_name] = df_out[tracer.qdoas_name]*1e-15
        df_out.to_csv(path.join(datadir, tracer_name+'_vcd_'+qdoas_file.replace('.ASC','.csv'),
                      sep=',', float_format='%.2f',
                      header=['Date_time (UTC)',
                              'VCD (geometric) 1e15 molecules cm-2',
                              'VCD 1e15 molecules cm-2'], index=False))
    return df
