# -*- coding: utf-8 -*-
"""
Created on Wed Jan 13 16:53:20 2021

@author: Vinod
"""

from os import path
import yaml
from toolbox.geoms_create_hdf import convert_qdoas2geoms


site = 'Mainz'
tracer_name = 'no2_vis'   # hcho, no2_vis
with open('configuration/config_' + site + '.yml') as conf:
    site_prop = yaml.safe_load(conf)
datadir = site_prop['datadir']
for period in ['2020_03']:
    qdoas_file = path.join(datadir, tracer_name+'_T2_' + period+'.ASC')
    convert_qdoas2geoms(qdoas_file, site, tracer_name)
